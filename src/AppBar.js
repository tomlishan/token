import React from 'react';
import styled, { css } from 'styled-components';
const Logo = styled.div`
  font-size:1.5em;
`
const ControllButton = styled.div`
  ${props => props.active && css`
    text-shadow:0px 0px 30px #03ff03
    font-size:.9em;
  `}

`
const Bar = styled.div`
  display:grid;
  margin-bottom:50px;
  grid-template-columns:200px auto 100px 100px;
`

export default function(){
    return <Bar>
            <Logo>
                Token
        </Logo>
            <div>
            </div>
            {!this.state.firstVisit && (
                <ControllButton
                    onClick={() => {
                        this.setState({ page: 'dashboard' })
                    }}
                    active={this.displayingDashboard()}>
                    Dashboard
        </ControllButton>)}
            <ControllButton
                onClick={() => {
                    this.setState({ page: 'settings' })
                }}
                active={this.displayingSettings()}>
                Settings
        </ControllButton>
        </Bar>
}