import React, { Component } from 'react';
import styled from 'styled-components';
import AppBar from './AppBar';
import CoinList from './CoinList';
import './App.css';

const cc = require('cryptocompare');
const AppLayout = styled.div`
  padding:50px;
`
const Content = styled.div`

`
const checkFirstVisit = () =>{
  let tokenDashData = localStorage.getItem('tokenDash')
  if (!tokenDashData){
    return {
      firstVisit:true,
      page:'settings'
    }
  }
  return {

  };
}

class App extends Component {
  state = {
    page:'dashboard',
    ...checkFirstVisit()
  }
  componentDidMount = () => {
    this.fetchCoins();
  }
  fetchCoins = async () => {
    let coinList = (await cc.coinList()).Data;
    this.setState({ coinList })
    console.log(this.state.coinList);
  }
  displayingDashboard = () =>  this.state.page === 'dashboard'
  displayingSettings = () =>  this.state.page === 'settings'
  firstVisitMessage = () =>{
    if(this.state.firstVisit){
      return <div>Welcome to tokendash please select your favorite coins to begin</div>
    }
  }
  favorites = () => {
    localStorage.setItem('tokenDash','test');
    this.setState({
      firstVisit:false,
      page: 'dashboard'
    })
  }
  settingscontent = () => {
    return <div>
    { this.firstVisitMessage() }
        <div onClick = { this.favorites }>
         confirm favorite
        </div> 
        <div>
        {CoinList.call(this)}
        </div>
      </div>
  }
  loadingContent = () => {
    if(!this.state.coinList){
      return <div> Loading coins</div>
    }
  }
  render() {
    return (
  <AppLayout>
      {AppBar.call(this)}
      {this.loadingContent()||<Content>
        {this.displayingSettings() && this.settingscontent()}
      </Content>}
  </AppLayout>
    );
  }
}

export default App;
